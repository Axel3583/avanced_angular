import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SocialMediaRoutingModule } from './social-media-routing.module';
import { PostService } from './service/posts.service';
import { PostListComponent } from './components/post-list/post-list.component';
import { PostResolver } from './resolver/post.resolver';
import { PostListItemComponent } from './components/post-list-item/post-list-item.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    PostListComponent,
    PostListItemComponent
  ],
  imports: [
    CommonModule,
    SocialMediaRoutingModule,
    SharedModule
  ],

  providers: [
    PostService,
    PostResolver
  ]
})
export class SocialMediaModule { }
