import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../model/post.model';
import { Observable } from 'rxjs';
import { env } from 'env/env';

@Injectable({
    providedIn: 'root'
})

export class PostService {
    constructor(private httpClient: HttpClient) { }
    
    getPost(): Observable<Post[]> {
        return this.httpClient.get<Post[]>(`${env.apiUrl}/posts`)
    }

    addNewCommented(postCommented: {comment: string, postId: number}) {
        console.log(postCommented, 'Axelll')
    }
}