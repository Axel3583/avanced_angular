import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../model/post.model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PostService } from '../service/posts.service';

@Injectable({providedIn: 'root'})

export class PostResolver implements Resolve<Post[]> {
    
    constructor(private post: PostService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Post[] | Observable<Post[]> {
        return this.post.getPost()
    }
    
}