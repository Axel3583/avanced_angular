import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Comment } from 'src/app/core/models/comment.model';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

@Input() comments!: Comment[]
@Output() newComment = new EventEmitter<Comment>();

commentCtrl!: FormControl<any>;

constructor(private frombuilder : FormBuilder){}

  ngOnInit(): void {
   this.commentCtrl = this.frombuilder.control('', [Validators.required, Validators.minLength(10)])
  }

  onLeaveComment() {
   this.newComment.emit(this.commentCtrl.value)
   this.commentCtrl.reset()
  }
  
}
